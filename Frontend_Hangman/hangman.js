let secretword, guessingProgress, lettersLog, attempts, letter;
let words = [];
checkButton = document.getElementById("check");
restartButton = document.getElementById("restart");
restartButton.addEventListener('click', restart);

window.onload = function () {
    getWordsFromApi();
    restart();
};

function getWordsFromApi() {
    const URL = 'https://restcountries.eu/rest/v2/region/Europe?fields=name;capital'
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: URL,
        async: false,
        success: function (response) {
            for (let i = 0; i < response.length; i++) {
                var obj = response[i];
                words.push(obj.name.replace(/ /g, "-").toUpperCase());
                words.push(obj.capital.replace(/ /g, "-").toUpperCase());
            }
        },
        error: function () {
            words = ["PIES", "KOT", "JAVA", "PROFILSOFTWARE", "PYTHON", "BACKEND", "FRONTEND", "BITBUCKET", "GITHUB", "PROGRAMMING"];
        }
    });
}

checkButton.addEventListener('click', function () {
    letter = document.getElementById('letter').value.toUpperCase();
    document.getElementById('letter').value = '';
    checkLetter();
});

function getWord() {
    return words[Math.floor(Math.random() * words.length)];
};

function restart() {
    checkButton.disabled = false;
    guessingProgress = [];
    lettersLog = [];
    attempts = 5;
    secretword = getWord();
    for (let i = 0; i < secretword.length; i++) {
        if (secretword[i] == '-' || secretword[i] == '.' || secretword[i] == '(' || secretword[i] == ')')
            guessingProgress[i] = secretword[i];
        else
            guessingProgress[i] = '_';
    }
    document.getElementById("lettersUsed").innerHTML = 'Letters used: ' + lettersLog;
    document.getElementById("attempts").innerHTML = 'Attempts left: ' + attempts;
    document.getElementById("guessWord").innerHTML = guessingProgress.join('');
}

function checkLetter() {
    found = false;
    if (lettersLog.indexOf(letter) != -1) {
        alert("Letter already used, try another");
        return;
    };
    for (let i = 0; i < guessingProgress.length; i++) {
        if (secretword[i] == letter) {
            guessingProgress[i] = letter;
            found = true;
        }
    }
    lettersLog.push(letter);
    if (!found) attempts--;
    if (attempts == 0) {
        checkButton.disabled = true;
        alert('You lose, secret word: ' + secretword);
    }
    if (guessingProgress.indexOf('_') == -1) {
        alert('NICE! Click restart to play again');
        checkButton.disabled = true;
    }
    document.getElementById("attempts").innerHTML = 'Attempts left: ' + attempts;
    document.getElementById("lettersUsed").innerHTML = 'Letters used: ' + lettersLog;
    document.getElementById("guessWord").innerHTML = guessingProgress.join('');
}