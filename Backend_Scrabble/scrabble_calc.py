import argparse
import sys

from calc import PointsCalc
from not_allowed_char_in_word_error import WrongCharInWordError


class ScrabbleCalc:

    def __init__(self):
        self.points_calc = PointsCalc()
        parser = argparse.ArgumentParser('Script do calculate value of given words')
        argument_group = parser.add_mutually_exclusive_group()

        argument_group.add_argument("--word", "-w", type=str, required=False, help='Check score of word, allowing '                                                                     'only letters')
        argument_group.add_argument("--file", "-f", type=str, required=False, help='Check best score of the file')
        argument_group.add_argument("--score", "-s", type=int, required=False, help='Check word with given score')
        self.args = parser.parse_args()

    def check_args(self):
        result = ''
        if len(sys.argv) <= 1:
            result = 'No args given, type scrabble.py -h to see help message'
        try:
            if self.args.word is not None:
                result = self.points_calc.calc_word_points(self.args.word)
            elif self.args.file is not None:
                result = self.points_calc.calc_max_from_file(self.args.file)
            elif self.args.score is not None:
                result = self.points_calc.check_word_with_score(self.args.score)
            self.points_calc.word_database_service.save_word_database()
            print(result)
        except (WrongCharInWordError, FileNotFoundError) as e:
            print(e)
