import os


class WordDatabaseService:

    def __init__(self):
        self.database_name = 'wordbase.txt'
        self.word_database = self.get_word_database()
        self.database_changed = False

    def get_word_database(self) -> dict:
        if not os.path.isfile(self.database_name):
            open(self.database_name, "w+")
            return {}
        with open(self.database_name, "r") as file:
            word_database = {}
            for line in file:
                word, score = line.strip().split('=')
                word_database[str(word)] = int(score)
            return word_database

    def word_score_from_database(self, word) -> int:
        score = self.word_database.get(word)
        if score is None:
            return -1
        return score

    def add_word_to_database(self, word, score):
        self.database_changed = True
        self.word_database[word] = score

    def save_word_database(self):
        if self.word_database and self.database_changed:
            with open(self.database_name, "w+") as file:
                for key, value in self.word_database.items():
                    file.write(key + "=" + str(value) + "\n")

    def all_word_with_score(self, score):
        return tuple(word for word, scorez in self.word_database.items() if scorez == score)
