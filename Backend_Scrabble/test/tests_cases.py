import unittest

from calc import PointsCalc
from not_allowed_char_in_word_error import WrongCharInWordError


class WordCalcTest(unittest.TestCase):
    def setUp(self):
        self.points_calc = PointsCalc()

    def test_calc_correct_word(self):
        tests = [
            ('PROFILSOFTWARE', 25),
            ('MATEUSZ', 18),
            ('TEST', 4),
        ]
        for word, score in tests:
            with self.subTest(value=word):
                self.assertEqual(self.points_calc.calc_word_points(word), score)

    def test_calc_wrong_word(self):
        tests = [
            '12313213',
            'MATE4usz',
             '!@#123zDOBRE',
        ]
        for wrongword in tests:
            with self.subTest(value=wrongword):
                with self.assertRaises(WrongCharInWordError):
                    self.points_calc.calc_word_points(wrongword)

    def test_check_small_letter(self):
        tests = [
            ('profilsoftware', 25),
            ('mateusz', 18),
            ('test', 4),
        ]
        for word, score in tests:
            with self.subTest(value=word):
                self.assertEqual(self.points_calc.calc_word_points(word), score)


class FileTopScoreTest(unittest.TestCase):
    def setUp(self):
        self.points_calc = PointsCalc()

    def test_calc_best_score_from_file(self):
        score = self.points_calc.calc_max_from_file('test_case_correct.txt')
        self.assertEqual(score, 29)
        score = self.points_calc.calc_max_from_file('test_case_correct2.txt')
        self.assertEqual(score, 23)
        tests = [
            ('test_case_correct.txt', 29),
            ('test_case_correct2.txt', 23),
        ]
        for file, score in tests:
            with self.subTest(value=file):
                self.assertEqual(self.points_calc.calc_max_from_file(file), score)

    def test_file_not_exists(self):
        with self.assertRaises(FileNotFoundError):
            self.points_calc.calc_max_from_file('not_existing_file.txt')
            self.points_calc.calc_max_from_file('not_existing_file')

    def test_file_only_wrong_words(self):
        score = self.points_calc.calc_max_from_file('test_case_only_wrong.txt')
        self.assertEqual(score, 0)


class WordWithGivenValueTest(unittest.TestCase):
    def setUp(self):
        self.points_calc = PointsCalc()

    def test_word_with_given_value_already_in_database(self):
        word = self.points_calc.check_word_with_score(25)
        self.assertEqual(word, 'PROFILSOFTWARE')

    def test_word_with_given_value_not_in_database(self):
        word = self.points_calc.check_word_with_score(133)
        self.assertEqual(word, '')

    def test_words_with_given_value_is_random(self):
        word = self.points_calc.check_word_with_score(19)
        self.assertIn(word, ('MATEUSZA', 'ROCKBURGER'))


if __name__ == '__main__':
    unittest.main()
