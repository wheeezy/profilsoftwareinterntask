from scrabble_calc import ScrabbleCalc


def main():
    scrabble_calc = ScrabbleCalc()
    scrabble_calc.check_args()


if __name__ == '__main__':
    main()
