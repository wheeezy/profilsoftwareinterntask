class WrongCharInWordError(RuntimeError):
    def __init__(self, message):
        super(WrongCharInWordError, self).__init__(message)
