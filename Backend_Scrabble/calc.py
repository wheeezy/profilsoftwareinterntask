import os
import random

from not_allowed_char_in_word_error import WrongCharInWordError
from word_database_service import WordDatabaseService


class PointsCalc:
    SCRABBLES_SCORES = [(1, "E A O I N R T L S U"), (2, "D G"), (3, "B C M P"),
                        (4, "F H V W Y"), (5, "K"), (8, "J X"), (10, "Q Z")]
    LETTER_SCORES = {letter: score for score, letters in SCRABBLES_SCORES
                     for letter in letters.split()}

    def __init__(self):
        self.word_database_service = WordDatabaseService()

    def calc_word_points(self, word) -> int:
        word = word.upper()
        score = self.word_database_service.word_score_from_database(word)
        if score is not -1:
            return score
        points_sum = 0
        for char in word:
            if not str(char).isupper():
                raise WrongCharInWordError('Found wrong char in word, cant calculate score of digits/special signs')
            points_sum += PointsCalc.LETTER_SCORES.get(char)
        self.word_database_service.add_word_to_database(word, points_sum)
        return points_sum

    def calc_max_from_file(self, file_name) -> int:
        if not os.path.isfile(file_name):
            raise FileNotFoundError('File not found')
        with open(file_name, "r") as file:
            best_score = 0
            for line in file:
                word = line[:-1]
                try:
                    word_score = self.calc_word_points(str(word))
                    if word_score > best_score:
                        best_score = word_score
                except WrongCharInWordError:
                    continue
            return best_score

    def check_word_with_score(self, score):
        words = self.word_database_service.all_word_with_score(score)
        if len(words) == 0:
            return ''
        return random.choice(words)
